package Lesson01;

public class Lesson01_Task01 {
    public static void main(String[] args) {
        int number = 12345;

        int lastDigit1 = number % 10;

        int number2 = number / 10;
        int lastDigit2 = number2 % 10;

        int number3 = number2 / 10;
        int lastDigit3 = number3 % 10;

        int number4 = number3 / 10;
        int lastDigit4 = number4 % 10;

        int number5 = number4 / 10;
        int lastDigit5 = number5 % 10;


        int digitsSum = lastDigit1 + lastDigit2 + lastDigit3 + lastDigit4 + lastDigit5;

        System.out.println("lastDigit1 = " + lastDigit1);
        System.out.println("lastDigit2 = " + lastDigit2);
        System.out.println("lastDigit3 = " + lastDigit3);
        System.out.println("lastDigit4 = " + lastDigit4);
        System.out.println("lastDigit5 = " + lastDigit5);

        System.out.println("Digits sum = " + digitsSum);

        }

    }